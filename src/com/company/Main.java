package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // 1.1 Дан список магазинов (название, адрес), которые занимаются продажей смартфонов. Каждый магазин содержит разное
        // кол-во смартфонов (от 2х до 6ти).
        //Необходимо найти:
        //самый дешевый смартфон среди всех доступных в магазинах (вывести всю инфу по телефону и название магазина)
        //все смартфоны заданного производителя (вывести всю инфу по телефону, названия и адреса магазинов)
        //смартфон с самой большой диагональю, если таких несколько - то выбрать самый дешевый из них (вывести всю инфу по смартфону)

        Shop[] shops = Generator.generate();
        //самый дешевый смартфон среди всех доступных в магазинах (вывести всю инфу по телефону и название магазина)
        System.out.println("Showing the cheapest phone we got in our shops");
        float minPrice = Shop.returnMinPrice(shops);
        for (Shop shop : shops) {
            for (Smartphone smartphone : shop.getSmartphones()) {
                if (smartphone.getPrice() == minPrice) {
                    Shop.printCheapestPhone(shop, smartphone);
                }
            }
        }
//
        System.out.println();
        //все смартфоны заданного производителя (вывести всю инфу по телефону, названия и адреса магазинов)
        System.out.println("Please fill the manufacturer you are interested in: ");
        Scanner scanner = new Scanner(System.in);
        String usersManufacturer = scanner.nextLine();

        for (Shop shop : shops) {
            for (Smartphone smartphone : shop.getSmartphones()) {
                if (smartphone.getCreatorName().equalsIgnoreCase(usersManufacturer)) {
                    Shop.printPhoneByManufacturer(shop, smartphone);
                }
            }
        }
        System.out.println();

        Smartphone[] phoneWithMinPriceAndBiggestSize = new Smartphone[100];
        float maxScreenSizeValue = Shop.returnMaxScreenSize(shops);
        //смартфон с самой большой диагональю, если таких несколько - то выбрать самый дешевый из них (вывести всю инфу по смартфону)
        System.out.println("Showing the cheapest/biggest smartphone we got in our shop : ");
        float minPriceForMaxSizeSmartphone = Integer.MAX_VALUE;
        for (Shop shop : shops) {
            for (Smartphone smartphone : shop.getSmartphones()) {
                if ((smartphone.getScreenSize() == maxScreenSizeValue) && smartphone.getPrice() <= minPriceForMaxSizeSmartphone) {
                    minPriceForMaxSizeSmartphone = smartphone.getPrice();
                    phoneWithMinPriceAndBiggestSize = shop.getSmartphones();
                }
            }
        }
        if (phoneWithMinPriceAndBiggestSize.length > 1) {
            for (Smartphone smartphone : phoneWithMinPriceAndBiggestSize) {
                if ((smartphone.getScreenSize() == maxScreenSizeValue) && (smartphone.getPrice() == minPriceForMaxSizeSmartphone)) {
                    Shop.printBiggestAndCheapestPhone(smartphone);
                }
            }
        } else {
            Shop.printBiggestAndCheapestPhone(phoneWithMinPriceAndBiggestSize[0]);
        }


    }
}
