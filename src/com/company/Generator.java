package com.company;

public class Generator {
    public static Shop[] generate() {
        Shop[] shops = new Shop[3];
        {
            Smartphone[] smartphones = new Smartphone[5];
            smartphones[0] = new Smartphone("LG", 4.7f, 322.4f);
            smartphones[1] = new Smartphone("LG", 4.8f, 122.4f);
            smartphones[2] = new Smartphone("Samsung", 4.0f, 341.4f);
            smartphones[3] = new Smartphone("Iphone", 5.7f, 922.4f);
            smartphones[4] = new Smartphone("Lenovo", 5.5f, 422.4f);

            shops[0] = new Shop("euroset", "Kiev", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[6];
            smartphones[0] = new Smartphone("LG", 5.7f, 3522.42131f);
            smartphones[1] = new Smartphone("LG", 5.8f, 1222.4f);
            smartphones[2] = new Smartphone("Samsung", 5.0f, 1341.4f);
            smartphones[3] = new Smartphone("Iphone", 2.7f, 922.4f);
            smartphones[4] = new Smartphone("Lenovo", 3.5f, 622.4f);
            smartphones[5] = new Smartphone("Lenovo", 4.8f, 322.4f);


            shops[1] = new Shop("mobilochka", "Poltava", smartphones);
        }
        {
            Smartphone[] smartphones = new Smartphone[2];
            smartphones[0] = new Smartphone("Rapsberry", 4.7f, 1300f);
            smartphones[1] = new Smartphone("Vertu", 5.8f, 10122.4f);

            shops[2] = new Shop("citrus", "Kharkiv", smartphones);
        }
        return shops;
    }
}
