package com.company;

public class Shop {
    private String shopName;
    private String address;
    private Smartphone[] smartphones;

    public Shop(String shopName, String address, Smartphone[] smartphones) {
        this.shopName = shopName;
        this.address = address;
        this.smartphones = smartphones;
    }

    public String getShopName() {
        return shopName;
    }

    public String getAddress() {
        return address;
    }

    public Smartphone[] getSmartphones() {
        return smartphones;
    }

    public static void printCheapestPhone(Shop shop, Smartphone smartphone) {
        System.out.println(String.format("Shop name is : %s , Manufacturer: %s, ScreenSize: %.2f, Price: %.2f",
                shop.shopName,
                smartphone.getCreatorName(),
                smartphone.getScreenSize(),
                smartphone.getPrice()));
    }

    public static void printPhoneByManufacturer(Shop shop, Smartphone smartphone) {
        System.out.println(String.format("Shop name is: %s, Shop address is: %s, Manufacturer: %s, ScreenSize: %.2f, Price: %.2f",
                shop.shopName,
                shop.address,
                smartphone.getCreatorName(),
                smartphone.getScreenSize(),
                smartphone.getPrice()));
    }

    public static void printBiggestAndCheapestPhone(Smartphone smartphone) {

        System.out.println(String.format("Manufacturer: %s, ScreenSize: %.2f, Price: %.2f",
                smartphone.getCreatorName(),
                smartphone.getScreenSize(),
                smartphone.getPrice()));

    }

    public static float returnMinPrice(Shop[] shops) {
        float minPrice = Integer.MAX_VALUE;
        for (Shop shop : shops) {
            for (Smartphone smartphone : shop.getSmartphones()) {
                if (smartphone.getPrice() != 0 && smartphone.getPrice() < minPrice) {
                    minPrice = smartphone.getPrice();
                }
            }
        }
        return minPrice;
    }

    public static float returnMaxScreenSize(Shop[] shops) {
        float maxScreenSize = Integer.MIN_VALUE;
        for (Shop shop : shops) {
            for (Smartphone smartphone : shop.getSmartphones()) {
                if (smartphone.getScreenSize() != 0 && smartphone.getScreenSize() > maxScreenSize) {
                    maxScreenSize = smartphone.getScreenSize();
                }
            }
        }
        return maxScreenSize;
    }
}
