package com.company;

public class Smartphone {
    private String creatorName;
    private float screenSize;
    private float price;

    public Smartphone(String creatorName, float screenSize, float price) {
        this.creatorName = creatorName;
        this.screenSize = screenSize;
        this.price = price;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public float getScreenSize() {
        return screenSize;
    }

    public float getPrice() {
        return price;
    }

    public static void print(Smartphone smartphone) {
        System.out.println(String.format("Manufacturer: %s , screensize: %s, price: %.2f", smartphone.creatorName, smartphone.screenSize, smartphone.price));
    }
}
